

	document.on("dom:loaded", function() {
      var editor = WysiHat.Editor.attach('content');
      var toolbar = new WysiHat.Toolbar(editor);
      toolbar.addButtonSet(WysiHat.Toolbar.ButtonSets.Basic);

      // Hide our error message if the editor loads fine
      $('error').hide();
    });
