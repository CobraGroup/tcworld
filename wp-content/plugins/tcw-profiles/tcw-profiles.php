<?php
/*
 Plugin Name: TC World Profiles
 Description: List of the profile for each location/office. @usage: [tc_profile loc="" limit=""]
 Version: 0.1 BETA
 Author: Naga Penmetsa
 Author URI: http://www.penmetsa.co.uk
 */

$tcw = new TC_Profiles();
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

class TC_Profiles {

	protected $loc_table = "tcw_locations";
	protected $profiles_table = "tcw_profiles";

	public function __construct()
	{
		global $wpdb;
		$this->loc_table = $wpdb->prefix. $this->loc_table; 
		$this->profiles_table = $wpdb->prefix. $this->profiles_table; 
		// showing in front end
		add_shortcode("tc_profile", array( $this, 'tc_profile_handler' ) );
		add_shortcode("tc_profile_view", array( $this, 'tc_profile_view_handler' ) );
		add_shortcode("del_loc", array( $this, 'delete_location' ) );
		add_shortcode("add_loc", array( $this, 'add_location' ) );
		add_shortcode("add_profile", array( $this, 'add_profile_fn' ) );
		add_shortcode("del_profile", array( $this, 'del_profile_fn' ) );
		add_shortcode("edit_profile", array( $this, 'edit_profile_fn' ) );
		add_shortcode("update_profile", array( $this, 'update_profile_fn' ) );

		// defining directory name
		define(MyPlugin_DIRNAME, 'tcw-profiles');

		// adding menu in backend
		add_action( 'admin_menu', array( $this, 'register_my_custom_menu_page' ));

		// showing locations
		add_shortcode('show_loc', array( $this, 'show_locations' ));
		
		// adding front end files
		add_action( 'admin_init', array( $this, 'your_css_and_js'));

		// locations crud actions
		
		//add_action('locations', $function_to_add)

	}
	
	function add_location($atts) {
		extract(shortcode_atts(array(
	        'loc_name' => '',
		), $atts));
		// Default usage.
		global $wpdb;
		$wpdb->insert( $this->loc_table, array( 'name' => $loc_name ) );	
	}
	
	function delete_location($atts) 
	{
		extract(shortcode_atts(array(
	        'loc_id' => 0,
		), $atts));
		// Default usage.
		global $wpdb;
		$wpdb->delete( $this->loc_table, array( 'ID' => $loc_id ) );	
	}
	
	function del_profile_fn($atts) 
	{
		extract(shortcode_atts(array(
	        'profile_id' => 0,
		), $atts));
		// Default usage.
		global $wpdb;
		$wpdb->delete( $this->profiles_table, array( 'ID' => $profile_id ) );
	}
	

	function edit_profile_fn($atts) {
		extract(shortcode_atts(array(
			'profile_id' => 0,
		), $atts));
		echo $this->edit_profile_form($profile_id);
		// Default usage.
		//global $wpdb;
		//$wpdb->insert( $this->profiles_table, array( 'location_id' => $location_id, 'name' => $name, 'bio' => $bio, 'division' => $division ) );
	}
	
	function update_profile_fn($atts) {
		extract(shortcode_atts(array(
			'profile_id' => 0,
			'name' => '',
			'bio' => '',
			'location_id' => 0,
			'division' => '',
			'photo_url' => '',
			'portrait' => '',
		), $atts));
		
		$data = array( 'name' => $name, 'bio' => $bio, 'division' => $division );
		
		if($portrait) {
			$data = array_merge($data ,array ('portrait' => $portrait));
		}
		if($photo_url) {
			$data = array_merge($data ,array ('photo_url' => $photo_url));
		}
		
		$where = array('id' => $profile_id);
		// Default usage.
		global $wpdb;
		
		$res = $wpdb->update( $this->profiles_table, $data, $where );
	}
	
	function add_profile_fn($atts) {
		extract(shortcode_atts(array(
	        'name' => '',
			'bio' => '',
			'location_id' => 0,
			'division' => '',
			'photo_url' => '',
			'portrait' => '',
		), $atts));
		// Default usage.
		global $wpdb;
		$wpdb->insert( $this->profiles_table, array( 'location_id' => $location_id, 'name' => $name, 'bio' => $bio, 'division' => $division,'photo_url' => $photo_url, 'portrait' => $portrait ) );
		
	}
	
	function admin_add_wysiwyg_custom_field_textarea()
{ 
	
echo  
'
<script type="text/javascript">/* <![CDATA[ */
	jQuery(function($){
		var i=1;
		$(\'.verve_meta_box_content textarea\').each(function(e)
		{
		  var id = $(this).attr(\'id\');
		  if (!id)
		  {
		   id = \'customEditor-\' + i++;
		   $(this).attr(\'id\',id);
		  }
		  tinyMCE.execCommand(\'mceAddControl\', false, id);
		});
	});
/* ]]> */</script>';
add_action( 'admin_print_footer_scripts', 'admin_add_wysiwyg_custom_field_textarea', 99 );
}

	
	function update_location($loc_id) {}

	function your_css_and_js() {
		wp_register_style('your_css_and_js', plugins_url('assets/css/bootstrap.css',__FILE__ ));
		wp_enqueue_style('your_css_and_js');
		wp_register_script( 'your_css_and_js', plugins_url('assets/js/bootstrap.js',__FILE__ ));
		wp_enqueue_script('your_css_and_js');
		wp_register_script( 'wysi', plugins_url('assets/js/ckeditor.js',__FILE__ ));
		wp_enqueue_script('wysi');
		//wp_register_script( 'wysi_b', plugins_url('wysihtml5/bootstrap-wysihtml5-0.0.2.min.js',__FILE__ ));
		//wp_enqueue_script('wysi_b');
		//wp_register_style( 'wysi_css', plugins_url('wysihtml5/bootstrap-wysihtml5-0.0.2.css',__FILE__ ));
		//wp_enqueue_style('wysi_css');
		wp_register_style( 'custom_css', plugins_url('assets/css/custom.css',__FILE__ ));
		wp_enqueue_style('custom_css');
		
	}

	function register_my_custom_menu_page(){
		add_menu_page( 'TCW', 'TC World Profiles', 3, MyPlugin_DIRNAME.'/backend.php', '', plugins_url(  MyPlugin_DIRNAME.'/assets/imgs/bullet_point.png' ), 4 );
	}

	public  function tc_profile_handler($atts) {

		extract(shortcode_atts(array(
	        'loc' => 'Head-office',
	        'loc_id' => 1,
	        'limit' => 50
		), $atts));

		//run function that actually does the work of the plugin
		return $this->tc_profile_function($loc_id, $limit);
	}
	
	public  function tc_profile_view_handler($atts) {

		extract(shortcode_atts(array(
	        'loc' => 'Head-office',
	        'loc_id' => '',
	        'limit' => 50
		), $atts));

		//run function that actually does the work of the plugin
		return $this->profiles_view($loc_id);
	}

	private function tc_profile_function($loc_id, $limit) {
		//process plugin
		$output = $this->show_profiles($loc_id);
		//send back text to calling function
		return $output;
	}


	public function show_locations()
	{
		global $wpdb;

		$locations = $wpdb->get_results( "SELECT * FROM ".$this->loc_table );
		$data = '<div class="span8">
			<p>&nbsp;</p>
			<p>&nbsp;</p>
					<table class="table">
					<tr>
					<th colspan="7">Locations</th>
					</tr><tr class="info" >';
		foreach ($locations as $location){

			$data .= '<td><a href="?page=tcw-profiles/backend.php&type=show&loc_id=' .$location->id.'">' .$location->name.'</a> &nbsp; 
			<!-- <a href="?page=tcw-profiles/backend.php&action=delete&loc_id=' .$location->id.'">delete</a> -->
			</td>';
		}
		$data .='</tr></table></div>'
		
		.$this->add_location_form();
		
		return $data;

	}
	
	private function profiles($loc_id)
	{
		global $wpdb;

		$sql = "SELECT p.*,l.name as location_name FROM ".$this->profiles_table." as p
		LEFT JOIN ".$this->loc_table." as l ON (p.location_id = l.id) ";
		if($loc_id){
			$sql .= "WHERE p.location_id = ".$loc_id ;
		} else {
			$sql .= "WHERE p.location_id != 0 AND p.location_id IS NOT NULL " ;
		}
		return $wpdb->get_results( $sql );
	}
	
	private function profile($profile_id)
	{
		global $wpdb;

		$sql = "SELECT p.*,l.name as location_name FROM ".$this->profiles_table." as p
		LEFT JOIN ".$this->loc_table." as l ON (p.location_id = l.id) WHERE p.id = ".$profile_id ;
		return $wpdb->get_row( $sql );
	}

	public function show_profiles($loc_id)
	{
		$profiles = $this->profiles($loc_id); 
		$data = 
		
		$this->add_profile_form().'<div class="span12">
					<h2>Profiles</h2>
					<table class="table">
					<tr>
					<th>Name</th>
					<th>Owner/Partner</th>
					<th>#action</th>
					</tr>
					';
		foreach ($profiles as $profile){

			$data .= '<tr>
			<td><a href="?page=tcw-profiles/backend.php&profile_id=' .$profile->id.'">' .$profile->name.'</a></td>
			<td>'.$profile->division.'</td>
			<td>
			<a href="?page=tcw-profiles/backend.php&action=edit&profile_id=' .$profile->id.'">edit</a> | 
			<a href="?page=tcw-profiles/backend.php&action=delete&profile_id=' .$profile->id.'&loc_id=' .$loc_id.'" onclick="javascript:return confirm(\'Are you sure you want to delete this profile?\')">delete</a>
			</td>
			</p>';
		}
		$data .='</table></div>';
		return $data;
	}
	
	public function profiles_view($loc_id)
	{
		
		$upload_dir = wp_upload_dir(); 
		
		
		$profiles = $this->profiles($loc_id);
		$data = '';
		if($loc_id) {
			$data .= '<h2>'.$profiles[0]->location_name.' team</h2>';	
		} else {
				$data .= '<h2> Our team</h2>';
		}
		$data .= ' 
		<div class="span8">';
		foreach ($profiles as $profile){
			
			$data .= '
			<!-- Button trigger modal -->
			<button data-toggle="modal" data-target="#profile-'.$profile->id.'" class="img-profile">';
			if($profile->photo_url) {
				$data .= '
				  <img src="'.plugin_dir_url(__FILE__).'uploads/'.$profile->photo_url.'"  alt="'.$profile->name.' photo" />';
			}
		    $data .= '
			  <p class="orange" style="padding: 10px 0 0 0; ">'.$profile->name.'<br/><span class="black">';
		    if($loc_id){
		    	$temp_var =  $profile->division;
		    } else {
		    	$temp_var =  $profile->location_name;
		    }
		    $data .= $temp_var.'</span></p></button>	
			
			<!-- Modal -->
			<div class="modal fade hide" id="profile-'.$profile->id.'" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-body clearfix">
				      <div class="img-portraits pull-left" style="width:38%; padding:1px">';
				      if($profile->portrait){
					      	$data .='<img src="'.plugin_dir_url(__FILE__).'uploads/'.$profile->portrait.'"  alt="'.$profile->name.' photo" />';
				      }
					      	$data .='<span class="modal-title orange" id="myModalLabel"><h4 class="orange">'.$profile->name.'</h4>'.$profile->location_name.',<br/> '.$profile->division.'</span>
				      </div>
				      <div class=" pull-right" style="width:60%">
 							'.strip_magic_slashes(base64_decode($profile->bio)).'
 							
				      </div>
			      </div>
			      <div class="modal-footer">
			      	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			      </div>
			    </div>
			  </div>
			</div>
			<!-- /. Model-->
			';
		}
		$data .='</div>';
		
		$html_warp = '';
		
		return $data;
	}
	
	public function add_profile_form()
	{
		$form = '<div class="span8"><h2>Add New Profile</h2>';
		$form .= '
		<form role="form" action="?page=tcw-profiles/backend.php&type=submit&profile=true" method="post" enctype="multipart/form-data">
			  <div class="form-group">
			    <label for="name">Name</label>
			    <input type="text" class="form-control" id="name" placeholder="Full name" name="name" required="required">
			  </div>
			  <div class="form-group">
			    <label for="division">Owner/Partner</label>
			    <input type="text" class="form-control" id="division" placeholder="" name="division" required="required">
			  </div>
			  <div class="form-group">
			    <label for="bio">Bio</label>
			    <textarea class="form-control ckeditor verve_meta_box_content" rows="3" placeholder="About yourself" id="bio" name="bio" required="required"></textarea>
			  </div>
			  <div class="form-group">
			    <label for="thumpnail">Thumbnail</label>
			    <input class="form-control ckeditor" rows="3" placeholder="About yourself" id="thumpnail" name="thumbnail" type="file" style="height: 60px;"/>
			  </div>
			  <div class="form-group">
			    <label for="portrait">Portrait</label>
			    <input class="form-control ckeditor" rows="3" placeholder="About yourself" id="portrait" name="portrait"  type="file" style="height: 60px;"/>
			  </div>
			  	<input type="hidden" name="location_id" value="'.$_GET['loc_id'].'">
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>';
		$form .= "</div>";
		return $form;
	}
		
	public function edit_profile_form($profile_id)
	{
		$profile = $this->profile($profile_id);
						
		$form = '<div class="span8"><h2>Edit '.$profile->name.' Profile</h2>';
		$form .= '
		<form role="form" action="?page=tcw-profiles/backend.php&action=update&profile_id='.$pr.'" method="post"  enctype="multipart/form-data">
			  <div class="form-group">
			    <label for="name">Name</label>
			    <input type="text" class="form-control" id="name" placeholder="Full name" name="name" value="'.$profile->name.'">
			  </div>
			  <div class="form-group">
			    <label for="division">Owner/Partner</label>
			    <input type="text" class="form-control" id="division" placeholder="" name="division" value="'.$profile->division.'">
			  </div>
			  <div class="form-group">
			    <label for="bio">Bio</label>
			    <textarea class="form-control ckeditor" rows="8" placeholder="About yourself" id="bio" name="bio">'.base64_decode($profile->bio).'</textarea>
			  </div>
			  <div class="form-group">
			    <label for="thumpnail">Thumbnail</label>
			    <input class="form-control ckeditor" rows="3" placeholder="About yourself" id="thumpnail" name="thumbnail" type="file" style="height: 60px;"/>';
		if($profile->photo_url) {
			$form .= '<img src="'.plugin_dir_url( __FILE__ ).'/uploads/'.$profile->photo_url.'" width="100"/>';
		}
		$form .= '</div>
			  <div class="form-group">
			    <label for="portrait">Portrait</label>
			    <input class="form-control ckeditor" rows="3" placeholder="About yourself" id="portrait" name="portrait"  type="file" style="height: 60px;"/>';
		if($profile->portrait) {
			$form .= '<img src="'.plugin_dir_url( __FILE__ ).'/uploads/'.$profile->portrait.'" width="100"/>';
		}
		$form .= ' </div>
			  	<input type="hidden" name="profile_id" value="'.$profile_id.'">
			  <button type="submit" class="btn btn-default">Update</button>
			</form>';
		$form .= "</div>";
		return $form;
		
	}
	
	public function add_location_form()
	{
		$form = '
		<div class="span12" ><p>&nbsp;</p></div>
		<div class="span6"><h2>Add New Location</h2>';
		$form .= '
		<form role="form" action="?page=tcw-profiles/backend.php&type=submit&loc=true" method="post">
			  <div class="form-group" >
			    <label for="loc_name">Location Name</label>
			    <input type="text" class="form-control" id="loc_name" placeholder="Full name" name="loc_name">
			  </div>
			  			  <button type="submit" class="btn btn-default">Submit</button>
			</form>';
		$form .= "</div>";
		return $form;
		
	}
}





register_activation_hook( __FILE__, 'installDatabaseTables' );
register_deactivation_hook( __FILE__, 'uninstallDatabaseTables' );

/**
 *
 * Called by install() to create any database tables if needed.
 * Best Practice:
 * (1) Prefix all table names with $wpdb->prefix
 * (2) make table names lower case only
 * @return void
 */
function uninstallDatabaseTables() {
	global $wpdb;

	$table_name1 = $wpdb->prefix ."tcw_locations";
	$table_name2 = $wpdb->prefix ."tcw_profiles";

	$sql1 = "DROP TABLE {$table_name1} ;";
	$sql2 = "DROP TABLE {$table_name2} ;";


	dbDelta( $sql1 );
	dbDelta( $sql2 );
}
function installDatabaseTables() {
	global $wpdb;

	$table_name1 = $wpdb->prefix ."tcw_locations";
	$table_name2 = $wpdb->prefix ."tcw_profiles";

	$sql1 = "CREATE TABLE {$table_name1} (
			  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
			  `name` varchar(55) DEFAULT NULL,
			  `url` varchar(255) DEFAULT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

	$sql2 = "CREATE TABLE {$table_name2} (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `location_id` int(11) NOT NULL DEFAULT '0',
		  `name` varchar(255) DEFAULT NULL,
		  `division` varchar(55) DEFAULT NULL,
		  `bio` text,
		  `photo_url` varchar(255) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql1 );
	dbDelta( $sql2);


}

function strip_magic_slashes($str) {   
	$str = str_replace('\"','',$str,$c1);
	$res = str_replace('\"','',$str,$c2);

	return $res; 
}