=== Visual Form Builder Pro ===
Contributors: mmuro
Requires at least: 3.3
Tested up to: 3.3.1
Stable tag: 1.3

Dynamically build forms using a simple interface. Forms include jQuery validation, a basic logic-based verification system, and entry tracking.

== Release Notes ==

**Version 1.3**

* Add Drag and Drop ability to add Form Items
* Add Plain Text email design option
* Add Additional Footer Text option
* Add option to remove footer link back
* Add Label Alignment option
* Add server side form validation; SPAM hardening
* Add inline Field help tooltip popups
* Update Form Settings UI
* Update File Upload field to place attachments in Media Library
* Update Field Description to allow HTML tags
* Update Field Name and CSS Classes to enforce a maxlength of 255 characters
* Fix bug preventing form deletion
* Fix bug preventing Custom Static Variable in Hidden Field
* Fix bug where Verification and Secret fields were displayed on Entries Detail page

**Version 1.2**

* Add Accepts option to File Upload field
* Add Small size to field options
* Add Options Layout to Radio and Checkbox fields
* Add Field Layout to field options
* Update jQuery in admin
* Verification fields now customizable
* Verification field now can be set to not required

**Version 1.1.1**

* Fix bug where adding fields was broken

**Version 1.1**

* Fix bug where assigning a price to PayPal did not save
* Minor updates to CSS
* Minor updates to database structure

**Version 1.0**

* 10 new Form Fields (Username, Password, Color Picker, Autocomplete, and more)
* Edit and Update Entries
* Quality HTML Email Template
* Email Designer
* Analytics
* Data & Form Migration
* PayPal Integration
* Form Paging
* No License Key
* Unlimited Use
* Automatic Updates
