
<?php

/*
Template Name: Contact us page
*/

 get_header() ?>
<?php display_my_banner() ?>
<?php ?>	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
echo '<div class="row">
<div class="span12">
';
the_content();
echo '</div>
</div>
';
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>

<?php get_footer() ?>
