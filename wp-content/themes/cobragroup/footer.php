
		<footer>
			<div class="footer">
			<div class="row ">
				<div class="span4 social" >
					<h4 class="white">Follow us on:</h4>
					<a href="https://www.facebook.com/TCWorldLtd" target="_blank"><span class="icon fb">facebook</span> </a>
					<a href="https://twitter.com/tcworldltduk" target="_blank" ><span class="icon tw">twitter</span> </a>
					<a href="http://uk.linkedin.com/in/tcworldltd" target="_blank" ><span class="icon in">linkedin</span> </a>
					
					<div class="copyright" >
					<p class="white">Copyright &copy; 2013 TC World Ltd. <br/> All Rights Reserved. Terms and conditions</p>
					</div>
				</div>
		 		<div class="span4 middle" >
					<h4 class="white">Join our Business Development and Sales Programme.</h4>
					<a class="blue-btn" href="<?php echo esc_url( get_permalink( get_page_by_title( "Apply online" ) ) ); ?>">Don't delay, apply online today</a>
					<p class="">&nbsp;</p>
					<p class="white">Our intensive product training programme is designed to help everyone to meet their full potential and acheive their goals.</p>
				</div>
				<div class="span4 appco">
					<h4 class="white">We are contracted to:</h4>
					<a href="http://appcogroup.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/appco_logo.png" /></a>
				</div>
				
			</div>
			</div>
		</footer>
    </div> <!--  /.w960 -->
</body>


<script type="text/javascript">
$(document).ready(function(){
    activeItem = $("#accordion li:first");
    $(activeItem).addClass('active');
 
    $("#accordion li").on('click',function(){
        $(activeItem).animate({width: "56px"}, {duration:300, queue:false});
        $(this).animate({width: "450px"}, {duration:300, queue:false});
        activeItem = this;
        $(this).addClass("active");
        $(this).siblings().removeClass("active");
    });
});
</script>
</html>