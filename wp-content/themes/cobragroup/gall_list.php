
<?php

/*
Template Name: List of galleries
*/

 get_header() ?>
 <div class="clearfix"></div>
<?php display_my_banner() ?>


<?php ?>	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
echo '<div class="row">
<h2>TC World Galleries</h2>
';
    $args = array( 'post_type' => 'gallery' );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
    	echo '<div class="span6">';
		echo do_shortcode('[print_gllr id='.get_the_ID().' display=short]');
		echo '</div>';
	endwhile; 

echo '
</div>
<div class="row">&nbsp;</div>
';
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>

<?php get_footer() ?>
