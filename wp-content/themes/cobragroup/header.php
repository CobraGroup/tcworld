<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' );?> <?php echo get_bloginfo('name'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<link rel="stylesheet"  type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" />
	<link rel="stylesheet"  type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" />
	<link rel="stylesheet"  type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/datepicker.css" />
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-1.11.0.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
	<script src="http://www.appelsiini.net/download/jquery.filestyle.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('.carousel').carousel({
				  interval: 2000,
				  wrap:false
				})
			// validate the comment form when it is submitted
			$(".form").validate({
				  submitHandler: function(form) {
				    form.submit();
				  }
				 });

			$(document).on('click', 'label', function(e) {
			    if(e.currentTarget === this && e.target.nodeName !== 'INPUT') {
			      $(this.control).click();
			    }
			  });


			$('.datepicker').datepicker({
				format:'mm/yyyy',
				viewMode:'years',
				minViewMode: 1
			})
		});
				  
	</script>
	
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.ico"/>
	<?php  wp_head(); ?>
	
	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
</head> <!-- /.head -->


<body>
<div class="w960">
	<div class="navbar navbar-inverse" role="navigation">
			<div class="container">
				<div class="pull-right" >
					<?php display_top_menu();?>
					<form class="navbar-form pull-right search-form" action="<?php bloginfo('url'); ?>/" id="searchform">
						<input class="span2 search-form-input" type="text" placeholder="Search" name="s" value="<?php the_search_query(); ?>"> 
						<input type="submit" value=" " id="search-btn" class="hidden-phone">
					</form>
				</div>
			</div>
		</div><!-- /.navbar-inverse -->
		
		
    	<div class="header">
    		
				<div class="logo pull-left">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" >
					<?php wp_title(); ?>
				</a>
	            </div>
            <div class="pull-right">
            	<h1 class="orange" id="mar-exp">Direct Marketing Experts</h1>
                <div class="phone"><span class="telephone-icon icon">phone</span>0161 923 6046</div>
            </div>
		</div><!-- /.header -->
		
		
		
		
		<div id="cssmenu">
<ul>
<?php display_main_menu(); ?>
</ul>
</div>
		