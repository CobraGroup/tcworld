<?php 

/*
Template Name: Home Page
*/



?>	<?php get_header() ?>
		
		<?php display_my_banner() ?>
		
		
			<div class="row">
			<div class="span9 matt-profile">
				<h2>Our business</h2>
				<p><img src="<?php echo get_template_directory_uri(); ?>/assets/img/matt.jpg"/ class="pull-left" >
				The story of TC World began with the incorporation of the very first marketing company in April 2003, and since that time the organisation has experienced incredible growth.</p>
				<p>
Today the organisation comprises of seven marketing companies across the UK representing some of the biggest brands in the world.
</p><p>
Year-on-year the list of clients that we are working with continues to grow at an impressive pace, allowing us to diversify into many new and exciting areas of the global market. Traditionally our focus has been in the telecommunication, financial service, utility, charity and insurance sectors but of course we are constantly researching new products and markets to continue our expansion.
</p><p>
Direct Marketing is now widely acknowledged as the most effective method of acquiring new customers and increasing brand awareness. This has forced many large companies to re-evaluate their traditional marketing strategies and outsource their direct marketing requirements to specialist organisations, like TC World.
</p><p>
One of our key objectives has always been to deliver a high standard of customer service to the end consumer and an excellent yet cost effective marketing campaign for the client.
</p><p>
Our long-term success is based on providing ambitious individuals in our organisation with opportunities to develop their skills, gain experience and ultimately progress to senior levels of responsibility. This can only be accomplished by continuing to deliver ongoing product training and support for our people, which will allow them in turn to achieve their own successes and assist in our continuing expansion plans.
</p>
<p>&nbsp;</p>
<p class="pull-left orange"> Matthew Bambroffe,<br/>Managing Director</p>			
			</div> <!-- /.span6 -->
			
			<!-- 
			<div class="span3">
				<div class="panel panel-primary">
				  <div class="panel-heading">
				    <h3 class="panel-title">TC World video</h3>
				  </div>
				  <div class="panel-body video"   data-toggle="modal" data-target="#video-link">
				  	<h4>Matthew's welcome</h4>
				  	<img src="http://img.youtube.com/vi/ScMzIvxBSi4/1.jpg" width="100%" />
				  	<span></span>
				  	<p>&nbsp;
				  	<p>Cum ne laboramus sententiae, mei ut dolor malorum detracto. Posidonium inciderint</p>
				  	<p>Cum ne laboramus sententiae, mei ut dolor malorum detracto.</p>
				  </div>
				</div>
				
				<div class="modal fade hide" id="video-link" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title orange" id="myModalLabel">Matthew's Welcome</h4>
				      </div>
				      <div class="modal-body">
				        <iframe width="520" height="300" src="//www.youtube.com/embed/ScMzIvxBSi4" frameborder="0" allowfullscreen></iframe>
				      </div>
				    </div>
				  </div>
				</div>
				
			</div>  -->
			<!-- /.span3 -->
			
			
			<div class="span3">
				<div class="panel panel-primary" id="carousel-panel">
				  <div class="panel-body" style="padding:0">
					  
				  	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
					    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
					  </ol>
					
					  <!-- Wrapper for slides -->
					  <div class="carousel-inner">
					  	<p class="orange">TC World is supporting:</p>
					    <div class="item active">
					    	<!-- <p  class="orange" >TC World Donates to:</p>  -->
					      <a href="http://www.thebraintumourcharity.org/" target="_blank" ><img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel/thebraintumourcharity.png" width="180" /></a>
					    </div>
					    <div class="item">
					      <a href="http://www.sah.org.uk" target="_blank" ><img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel/st-anns-logo.png" width="180" /></a>
					    </div>
					    <div class="item">
					      <a href="http://www.lynz-mybraintumour.org.uk/" target="_blank" ><img src="<?php echo get_template_directory_uri(); ?>/assets/img/carousel/brain-tumour.png" width="180" /></a>
					    </div>
					   
					  </div>
					
					  <!-- Controls -->
					  <a class="left-carousel-control  pull-left" href="#carousel-example-generic" data-slide="prev">
					    <img src="/wp-content/themes/cobragroup/assets/img/carosel_left_button_150.png" />
					 	 </a>
					   <a class="right-carousel-control  pull-right" href="#carousel-example-generic" data-slide="next">
					    <img src="/wp-content/themes/cobragroup/assets/img/carosel_right_button_150.png" />
					 	 </a>
					</div>
				  </div>
				</div>
				
						<div class="panel panel-primary twitter-widget">
					<div class="panel-heading">
					<h3 class="panel-title">Twitter feeds</h3>
					</div>
					<div class="panel-body">
					<?php echo do_shortcode('[kebo_tweets count="5"]') ?>
					</div>
				</div><!-- /.panel -->
				
				
			</div> <!-- /.span3 -->
			
		</div> <!-- /.row -->
		
		
		<?php get_footer() ?>
		
		