
<?php

/*
Template Name: Page with twitter feeds
*/

get_header() ?>
<?php

display_my_banner(); ?>
<?php ?>	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
echo '<div class="row">
<div class="span9">
';
the_content();
echo '</div>

<div class="span3">
<div class="panel panel-primary twitter-widget">
	<div class="panel-heading">
	<h3 class="panel-title">Twitter feeds</h3>
	</div>
	<div class="panel-body">
	'.do_shortcode('[kebo_tweets count="5"]').'
	</div>
</div><!-- /.panel -->
</div><!-- /.span3 -->
</div>

<!--

-->
';
endwhile; else: ?>
<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>

<?php get_footer() ?>
