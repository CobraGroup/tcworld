<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Shape
 * @since Shape 1.0
 */

get_header(); ?>

        <section id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
            
                    <h2 class="page-title"><?php printf( __( 'Search results for: %s', 'shape' ), '<span>' . get_search_query() . ' ( Total:'.$wp_query->post_count.' results)</span>' ); ?></h2>
            <?php if ( have_posts() ) : ?>

                <?php /* Start the Loop */ ?>
                <table class="table table-striped">
                <?php while ( have_posts() ) : the_post(); ?>
				<tr><td>
            	<a href="<?php the_permalink() ?>">
					  <h5 class="orange"><?php the_title(); ?></h5>
				</a></td>
				</tr>
                <?php endwhile; ?>
				</table>

            <?php else : ?>

                <p>No results found. Please try using different keyword</p>

            <?php endif; ?>

            </div><!-- #content .site-content -->
        </section><!-- #primary .content-area -->

<?php get_footer(); ?>