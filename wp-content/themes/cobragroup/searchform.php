<?php
/*
 Template Name: Search Page
 */

 get_header() ?>
 <div class="clearfix"></div>

<?php 
echo '<div class="row">
<div class="span12">
';
echo get_search_form();
echo '</div>
</div>
';
?>
<?php get_footer() ?>
